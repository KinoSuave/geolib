/**
 * Determines if setA and setB contain all of the same elements.
 */
export const setsEqual = <T>(setA: Set<T>, setB: Set<T>): boolean => {
	if (setA.size !== setB.size) {
		return false;
	}

	return [...setA].every((x) => setB.has(x));
};

/**
 * Returns a set representing the union of setA and setB.
 */
export const union = <T>(setA: Set<T>, setB: Set<T>): Set<T> => {
	return new Set([...setA, ...setB]);
};

/**
 * Returns a set representing the intersection of setA and setB.
 */
export const intersection = <T>(setA: Set<T>, setB: Set<T>): Set<T> => {
	return new Set([...setA].filter((x) => setB.has(x)));
};

/**
 * Returns a set representing setA - setB.
 */
export const difference = <T>(setA: Set<T>, setB: Set<T>): Set<T> => {
	return new Set([...setA].filter((x) => !setB.has(x)));
};

/** Adds multiple elements to an existing set. */
export const addMultipleToSet = <T>(items: Iterable<T>, set: Set<T>): void => {
	for (const item of items) {
		set.add(item);
	}
};
