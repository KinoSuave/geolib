import "jasmine";
import * as GeoSet from "./set";

describe("GeoSet setsEqual", () => {
	const equalSet1 = new Set([1, 6, 89, 3, 6, 2, 8, 0]);
	const equalSet2 = new Set(equalSet1);

	const unequalSet1 = new Set();
	const unequalSet2 = new Set([1, 6, 66, 3, 6, 2, 8, 0]);

	it("should determine properly if sets are equal", () => {
		expect(GeoSet.setsEqual(equalSet1, equalSet1)).toBe(true);
		expect(GeoSet.setsEqual(equalSet1, equalSet2)).toBe(true);
		expect(GeoSet.setsEqual(equalSet2, equalSet1)).toBe(true);
		expect(GeoSet.setsEqual(new Set(), new Set())).toBe(true);

		expect(GeoSet.setsEqual(equalSet1, unequalSet1)).toBe(false);
		expect(GeoSet.setsEqual(equalSet1, unequalSet2)).toBe(false);
	});
});

describe("GeoSet union", () => {
	it("should properly calculate a union", () => {
		const a = new Set([1, 2, 3]);
		const b = new Set([4, 3, 2]);

		const expectedResult = new Set([1, 2, 3, 4]);

		expect(GeoSet.setsEqual(GeoSet.union(a, b), expectedResult)).toBe(true);
		expect(GeoSet.setsEqual(GeoSet.union(a, new Set()), a)).toBe(true);
	});
});

describe("GeoSet intersection", () => {
	it("should properly calculate an intersection", () => {
		const a = new Set([1, 2, 3]);
		const b = new Set([4, 3, 2]);

		const expectedResult = new Set([2, 3]);

		expect(GeoSet.setsEqual(GeoSet.intersection(a, b), expectedResult)).toBe(
			true
		);
		expect(GeoSet.setsEqual(GeoSet.intersection(a, new Set()), new Set())).toBe(
			true
		);
	});
});

describe("GeoSet difference", () => {
	it("should properly calculate difference", () => {
		const a = new Set([1, 2, 3]);
		const b = new Set([4, 3, 2]);

		const aMinusB = new Set([1]);
		const bMinusA = new Set([4]);

		expect(GeoSet.setsEqual(GeoSet.difference(a, b), aMinusB)).toBe(true);
		expect(GeoSet.setsEqual(GeoSet.difference(b, a), bMinusA)).toBe(true);

		expect(GeoSet.setsEqual(GeoSet.difference(a, new Set()), a)).toBe(true);
		expect(GeoSet.setsEqual(GeoSet.difference(new Set(), a), new Set())).toBe(
			true
		);
	});
});
