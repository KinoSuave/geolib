import "jasmine";
import * as GeoArray from "./array";

describe("GeoArray equality", () => {
	const equalArray1 = [1, 6, 89, 3, 6, 2, 8, 0];
	const equalArray2 = equalArray1.slice(0);

	const unequalArray1: number[] = [];

	const unequalArray2 = equalArray1.slice(0);
	unequalArray2[unequalArray2.length - 1] = 10;

	it("should determine properly if arrays are equal", () => {
		expect(GeoArray.arraysEqual(equalArray1, equalArray1)).toBe(true);
		expect(GeoArray.arraysEqual(equalArray1, equalArray2)).toBe(true);
		expect(GeoArray.arraysEqual(equalArray2, equalArray1)).toBe(true);

		expect(GeoArray.arraysEqual(equalArray1, unequalArray1)).toBe(false);
		expect(GeoArray.arraysEqual(equalArray1, unequalArray2)).toBe(false);
	});
});

describe("GeoArray clone array", () => {
	const testArray = [4, 7, 1, 5, 9, 6, 6, 3];

	it("should clone array properly", () => {
		const clonedArray = GeoArray.cloneArray(testArray);

		expect(testArray).not.toBe(clonedArray);
		expect(GeoArray.arraysEqual(testArray, clonedArray)).toBe(true);

		testArray[0] = 9;
		expect(GeoArray.arraysEqual(testArray, clonedArray)).toBe(false);
	});
});

describe("GeoArray insertAt", () => {
	it("should insert elements properly", () => {
		const startingArray = [4, 9, 2];

		GeoArray.insertAt(startingArray, 1, 90);

		expect(startingArray.length).toBe(4);

		expect(startingArray[0]).toBe(4);
		expect(startingArray[1]).toBe(90);
		expect(startingArray[2]).toBe(9);
		expect(startingArray[3]).toBe(2);

		GeoArray.insertAt(startingArray, 2, 8, 13, 22);

		expect(startingArray.length).toBe(7);

		expect(startingArray[0]).toBe(4);
		expect(startingArray[1]).toBe(90);
		expect(startingArray[2]).toBe(8);
		expect(startingArray[3]).toBe(13);
		expect(startingArray[4]).toBe(22);
		expect(startingArray[5]).toBe(9);
		expect(startingArray[6]).toBe(2);
	});
});

describe("GeoArray sorted insert", () => {
	type SortedArrayObject = {
		sortProperty: number;
		nonSortProperty: string;
	};

	const a: SortedArrayObject = {
		sortProperty: 1,
		nonSortProperty: "apple",
	};

	const b: SortedArrayObject = {
		sortProperty: 2,
		nonSortProperty: "banana",
	};

	const g: SortedArrayObject = {
		sortProperty: 3,
		nonSortProperty: "grape",
	};

	const o: SortedArrayObject = {
		sortProperty: 3,
		nonSortProperty: "orange",
	};

	const p: SortedArrayObject = {
		sortProperty: 3,
		nonSortProperty: "pear",
	};

	const r: SortedArrayObject = {
		sortProperty: 4,
		nonSortProperty: "raspberry",
	};

	const s: SortedArrayObject = {
		sortProperty: 5,
		nonSortProperty: "strawberry",
	};

	const t: SortedArrayObject = {
		sortProperty: 7,
		nonSortProperty: "turnip",
	};

	const test: SortedArrayObject = {
		sortProperty: 3,
		nonSortProperty: "test",
	};

	const sortFunction = (aSort: SortedArrayObject, bSort: SortedArrayObject) => {
		if (aSort.sortProperty < bSort.sortProperty) {
			return -1;
		}

		if (aSort.sortProperty > bSort.sortProperty) {
			return 1;
		}

		return 0;
	};

	const toBeginningArray: SortedArrayObject[] = [r, s, t];
	const toEndArray: SortedArrayObject[] = [a, b];

	const toMiddleArray: SortedArrayObject[] = [a, b, g, o, p, r, s, t];

	it("should do beginning and end pushing properly", () => {
		const toBeginningArrayPush = GeoArray.cloneArray(toBeginningArray);
		const toEndArrayPush = GeoArray.cloneArray(toEndArray);

		const toBeginningArrayUnshift = GeoArray.cloneArray(toBeginningArray);
		const toEndArrayUnshift = GeoArray.cloneArray(toEndArray);

		GeoArray.sortedPush(toBeginningArrayPush, test, sortFunction);
		GeoArray.sortedUnshift(toBeginningArrayUnshift, test, sortFunction);

		expect(toBeginningArrayPush[0]).toBe(test);
		expect(toBeginningArrayUnshift[0]).toBe(test);

		for (let i = 1; i < toBeginningArrayPush.length; i++) {
			expect(toBeginningArrayPush[i]).toBe(toBeginningArray[i - 1]);
			expect(toBeginningArrayUnshift[i]).toBe(toBeginningArray[i - 1]);
		}

		GeoArray.sortedPush(toEndArrayPush, test, sortFunction);
		GeoArray.sortedUnshift(toEndArrayUnshift, test, sortFunction);

		expect(toEndArrayPush[toEndArrayPush.length - 1]).toBe(test);
		expect(toEndArrayUnshift[toEndArrayUnshift.length - 1]).toBe(test);

		for (let i = 0; i < toEndArray.length; i++) {
			expect(toEndArrayPush[i]).toBe(toEndArray[i]);
			expect(toEndArrayUnshift[i]).toBe(toEndArray[i]);
		}
	});

	it("should do sorted push in the middle properly", () => {
		const toMiddleArrayPush = GeoArray.cloneArray(toMiddleArray);

		GeoArray.sortedPush(toMiddleArrayPush, test, sortFunction);

		expect(toMiddleArrayPush.length).toBe(toMiddleArray.length + 1);

		let toMiddleArrayIndex = 0;
		for (let i = 0; i < toMiddleArrayPush.length; i++) {
			const value = toMiddleArrayPush[i] as SortedArrayObject;

			if (
				value.sortProperty === test.sortProperty &&
				(toMiddleArrayPush[i + 1] as SortedArrayObject).sortProperty !==
					test.sortProperty
			) {
				expect(value).toBe(test);
			} else {
				expect(value).toBe(
					toMiddleArray[toMiddleArrayIndex] as SortedArrayObject
				);
				toMiddleArrayIndex++;
			}
		}
	});

	it("should do sorted unshift in the middle properly", () => {
		const toMiddleArrayUnshift = GeoArray.cloneArray(toMiddleArray);

		GeoArray.sortedUnshift(toMiddleArrayUnshift, test, sortFunction);

		expect(toMiddleArrayUnshift.length).toBe(toMiddleArray.length + 1);

		let testValueSeen = false;
		let toMiddleArrayIndex = 0;
		toMiddleArrayUnshift.forEach((value) => {
			if (value.sortProperty === test.sortProperty && !testValueSeen) {
				expect(value).toBe(test);
				testValueSeen = true;
			} else {
				expect(value).toBe(
					toMiddleArray[toMiddleArrayIndex] as SortedArrayObject
				);
				toMiddleArrayIndex++;
			}
		});
	});
});

describe("GeoArray grouped", () => {
	it("should group properly for arrays equal to block size", () => {
		const testArray = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"];

		const result = GeoArray.group(testArray, 5);
		expect(result.length).toBe(2);

		const expectedResult = [
			["a", "b", "c", "d", "e"],
			["f", "g", "h", "i", "j"],
		];

		for (let i = 0; i < result.length; i++) {
			expect(
				GeoArray.arraysEqual(
					result[i] as string[],
					expectedResult[i] as string[]
				)
			).toBeTruthy();
		}
	});

	it("should group properly for arrays larger than block size", () => {
		const testArray = [
			"a",
			"b",
			"c",
			"d",
			"e",
			"f",
			"g",
			"h",
			"i",
			"j",
			"k",
			"l",
		];

		const result = GeoArray.group(testArray, 5);
		expect(result.length).toBe(3);

		const expectedResult = [
			["a", "b", "c", "d", "e"],
			["f", "g", "h", "i", "j"],
			["k", "l"],
		];

		for (let i = 0; i < result.length; i++) {
			expect(
				GeoArray.arraysEqual(
					result[i] as string[],
					expectedResult[i] as string[]
				)
			).toBeTruthy();
		}
	});

	it("should group properly for arrays smaller than block size", () => {
		const testArray = ["a", "b", "c", "d", "e", "f", "g"];

		const result = GeoArray.group(testArray, 10);
		expect(result.length).toBe(1);

		const expectedResult = [["a", "b", "c", "d", "e", "f", "g"]];

		for (let i = 0; i < result.length; i++) {
			expect(
				GeoArray.arraysEqual(
					result[i] as string[],
					expectedResult[i] as string[]
				)
			).toBeTruthy();
		}
	});

	it("should contain nothing when given an empty array", () => {
		const result = GeoArray.group([], 5);

		expect(result.length).toBe(0);
	});

	it("should throw if block size is 0 or less", () => {
		let thrown = false;

		try {
			GeoArray.group(["a", "b", "c"], -1);
		} catch {
			thrown = true;
		}

		expect(thrown).toBeTruthy();
	});
});
