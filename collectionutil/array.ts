import { isNumberAndNotNaN } from "../types/types";

/**
 * Returns true if two arrays are the same length and have equal values.
 *
 * @param array1 First array to compare
 * @param array2 Second array to compare
 */
export const arraysEqual = <T>(array1: T[], array2: T[]): boolean => {
	if (array1 === array2) {
		return true;
	}

	if (array1.length !== array2.length) {
		return false;
	}

	const iEnd = array1.length;
	for (let i = 0; i < iEnd; i++) {
		if (array1[i] !== array2[i]) {
			return false;
		}
	}

	return true;
};

/**
 * Returns a cloned shallow copy of the given array.
 *
 * @param array Array to clone
 */
export const cloneArray = <T>(array: T[]): T[] => {
	// This is pretty simple, but using cloneArray() instead of simply array.slice() clarifies intent
	return array.slice(0);
};

/**
 * Inserts one or more values into an array at a given index.
 *
 * @param array Array to alter
 * @param index Index to insert values at
 * @param values Values to insert into the array
 */
export const insertAt = <T>(array: T[], index: number, ...values: T[]) => {
	array.splice(index, 0, ...values);
};

/**
 * Removes elements from an array matching the given matching function. Array will be modified in-place.
 *
 * @param array Array to remove instances from
 * @param matcher Function that will receive an element. Return true to remove element, false to keep it.
 * @param maxElements Maximum number of elements to remove. Elements occuring earlier in the array are removed first. Defaults to no limit.
 * @returns An array of elements that were removed from the input array.
 */
export const removeElementsMatching = <T>(
	array: T[],
	matcher: (element: T) => boolean,
	maxElements?: number | undefined
): T[] => {
	const useMaxElements = isNumberAndNotNaN(maxElements);
	const result: T[] = [];

	if (useMaxElements && maxElements <= 0) {
		return result;
	}

	let instancesRemoved = 0;

	for (let i = 0; i < array.length; i++) {
		if (matcher(array[i] as T)) {
			result.push(array[i] as T);
			array.splice(i, 1);
			i--;

			if (useMaxElements) {
				instancesRemoved++;

				if (instancesRemoved >= maxElements) {
					return result;
				}
			}
		}
	}

	return result;
};

/**
 * Inserts a value into a sorted array. Performs the equivalent of array.push(value) followed by a stable sort with the given compare function,
 * but much more effiently given that we know that the array is already sorted. Array will be modified in-place.
 *
 * @param sortedArray Array that already has its values sorted according to the given compare function.
 * @param value Value to be inserted into the array.
 * @param compareFunction Function that defines the sort order.
 */
export const sortedPush = <T>(
	sortedArray: T[],
	value: T,
	compareFunction: (a: T, b: T) => number = defaultCompare
) => {
	sortedInsert(sortedArray, value, compareFunction, false);
};

/**
 * Inserts a value into a sorted array. Performs the equivalent of array.unshift(value) followed by a stable sort with the given compare function,
 * but much more effiently given that we know that the array is already sorted. Array will be modified in-place.
 *
 * @param sortedArray Array that already has its values sorted according to the given compare function.
 * @param value Value to be inserted into the array.
 * @param compareFunction Function that defines the sort order.
 */
export const sortedUnshift = <T>(
	sortedArray: T[],
	value: T,
	compareFunction: (a: T, b: T) => number = defaultCompare
) => {
	sortedInsert(sortedArray, value, compareFunction, true);
};

const defaultCompare = (a: any, b: any) => {
	if (a > b) {
		return 1;
	}

	if (a < b) {
		return -1;
	}

	return 0;
};

const sortedInsert = <T>(
	sortedArray: T[],
	value: T,
	compareFunction: (a: T, b: T) => number,
	onNextEqualInsert: boolean
) => {
	const iEnd = sortedArray.length;
	for (let i = 0; i < iEnd; i++) {
		const nextValue = sortedArray[i] as T;

		const compareResult = compareFunction(value, nextValue);

		if (compareResult < 0 || (compareResult === 0 && onNextEqualInsert)) {
			insertAt(sortedArray, i, value);
			return;
		}
	}

	sortedArray.push(value);
};

/**
 * Breaks an array into fixed size blocks.
 *
 * @param arr Array to break into blocks.
 * @param size Maximum size of each block.
 */
export const group = <T>(arr: T[], size: number): T[][] => {
	size = Math.floor(size);

	if (size <= 0) {
		throw new Error("Size cannot be 0 or less");
	}

	const result: T[][] = [];
	const numCollections = Math.ceil(arr.length / size);

	for (let collectionI = 0; collectionI < numCollections; collectionI++) {
		const collection: T[] = [];

		const startingI = collectionI * size;
		const endingI = Math.min((collectionI + 1) * size, arr.length);
		for (let i = startingI; i < endingI; i++) {
			collection.push(arr[i] as T);
		}

		result.push(collection);
	}

	return result;
};

/**
 * Shuffles an array in-place using Fisher-Yates shuffle. Returns the array.
 *
 * @param arr - Array to shuffle.
 */
export const shuffle = <T>(arr: T[]): T[] => {
	for (let i = arr.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		const temp = arr[i] as T;
		arr[i] = arr[j] as T;
		arr[j] = temp;
	}

	return arr;
};

/**
 * Gets a random element from an array. Array must have greater than 0 length.
 *
 * @param arr - Array to retrieve a random element of.
 */
export const randomFrom = <A>(arr: A[]): A => {
	if (arr.length === 0) {
		throw new Error("Cannot choose random element of empty array");
	}

	const chosenIndex = Math.floor(Math.random() * arr.length);
	return arr[chosenIndex] as A;
};
