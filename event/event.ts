import { EventId } from "./eventid";

/**
 * Event class ported from Brant's custom ES5 Event package, used originally in Hammer
 * applications.
 */
export class Event<T = undefined> {
	private defaultPrevented = false;
	private propagationStopped = false;

	/** Creates a new event with the specified type */
	constructor(public readonly type: EventId<Event<any>>, public readonly target: T) {}

	// Cancels an event's default behavior if that behavior can be canceled.
	public preventDefault() {
		this.defaultPrevented = true;
	}

	// Checks whether the preventDefault() method has been called on the event.
	public isDefaultPrevented(): boolean {
		return this.defaultPrevented;
	}

	// Prevents processing of any subsequent event listener in the event flow.
	public stopPropagation() {
		this.propagationStopped = true;
	}

	// Checks whether the stopPropagation() method has been called on the event.
	public isPropagationStopped(): boolean {
		return this.propagationStopped;
	}
}
