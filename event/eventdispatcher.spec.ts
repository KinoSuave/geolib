/* eslint-disable max-classes-per-file */
import "jasmine";
import { EventDispatcher } from "./eventdispatcher";
import { Event } from "./event";
import { EventId, generateEventId } from "./eventid";

type TestEventTargetType = Record<string, unknown> | undefined;
class TestEvent extends Event<TestEventTargetType> {
	constructor(type: EventId<TestEvent>, target?: TestEventTargetType) {
		super(type, target);
	}

	public static readonly TEST_EVENT = generateEventId<TestEvent>("TEST_EVENT");
	public static readonly NOT_DISPATCHED_EVENT = generateEventId<TestEvent>("NOT_DISPATCHED_EVENT");
}

class TestEventDispatcher extends EventDispatcher<TestEvent> {}

describe("EventDispatcher", () => {
	it("should dispatch events properly", () => {
		const testDispatcher = new TestEventDispatcher();
		const testTarget = {};

		const listenerSpyObj = {
			listener: (event: TestEvent) => {
				expect(event.type).toBe(TestEvent.TEST_EVENT);
				expect(event.target).toBe(testTarget);
				expect(event.isDefaultPrevented()).toBe(false);
				expect(event.isPropagationStopped()).toBe(false);
			},
		};

		spyOn(listenerSpyObj, "listener").and.callThrough();

		expect(testDispatcher.hasEventListener(TestEvent.TEST_EVENT)).toBe(false);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener);
		expect(testDispatcher.hasEventListener(TestEvent.TEST_EVENT)).toBe(true);

		const testEvent = new TestEvent(TestEvent.TEST_EVENT, testTarget);

		testDispatcher.dispatchEvent(testEvent);

		expect(listenerSpyObj.listener).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener).toHaveBeenCalledWith(testEvent);

		testDispatcher.dispatchEvent(testEvent);
		testDispatcher.dispatchEvent(testEvent);

		expect(listenerSpyObj.listener).toHaveBeenCalledTimes(3);
	});

	it("should remove all listener references on dispose", () => {
		const testDispatcher = new TestEventDispatcher();

		expect(testDispatcher.isDisposed()).toBe(false);

		const listener = () => fail("This shouldn't be called");

		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listener);
		testDispatcher.dispose();

		expect(testDispatcher.isDisposed()).toBe(true);
		expect(testDispatcher.hasEventListener(TestEvent.TEST_EVENT)).toBe(false);
	});

	it("should dispatch multiple events in the correct order", () => {
		const testDispatcher = new TestEventDispatcher();

		let listener1Called = false;
		let listener2Called = false;
		let listener3Called = false;
		let listenerTopCalled = false;

		const listenerSpyObj = {
			listener1: () => {
				expect(listenerTopCalled).toBe(true);
				listener1Called = true;
			},
			listener2: () => {
				expect(listener1Called).toBe(true);
				listener2Called = true;
			},
			listener3: () => {
				expect(listener2Called).toBe(true);
				listener3Called = true;
			},
			listenerTopPriority: () => {
				expect(listenerTopCalled).toBe(false);
				expect(listener1Called).toBe(false);
				listenerTopCalled = true;
			},
		};

		spyOn(listenerSpyObj, "listener1").and.callThrough();
		spyOn(listenerSpyObj, "listener2").and.callThrough();
		spyOn(listenerSpyObj, "listener3").and.callThrough();
		spyOn(listenerSpyObj, "listenerTopPriority").and.callThrough();

		testDispatcher.addEventListener(TestEvent.NOT_DISPATCHED_EVENT, listenerSpyObj.listener1);
		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).not.toHaveBeenCalled();

		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener2);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listenerTopPriority, 10);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener3);

		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener3).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listenerTopPriority).toHaveBeenCalledTimes(1);

		expect(listener1Called).toBe(true);
		expect(listener2Called).toBe(true);
		expect(listener3Called).toBe(true);
		expect(listenerTopCalled).toBe(true);
	});

	it("should allow listeners to be removed", () => {
		const testDispatcher = new TestEventDispatcher();

		const listenerSpyObj = {
			listener1: () => {},
			listener2: () => {},
			listener3: () => {},
		};

		spyOn(listenerSpyObj, "listener1").and.callThrough();
		spyOn(listenerSpyObj, "listener2").and.callThrough();
		spyOn(listenerSpyObj, "listener3").and.callThrough();

		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1);
		const listener2Key = testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener2);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener3);

		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener3).toHaveBeenCalledTimes(1);

		testDispatcher.removeListenerByKey(listener2Key);
		testDispatcher.removeEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1);

		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener3).toHaveBeenCalledTimes(2);
	});

	it("handles removal of identical listeners properly, in order of highest priority", () => {
		const testDispatcher = new TestEventDispatcher();
		let listener1Called = false;
		let expectListener1Called = true;

		const listenerSpyObj = {
			listener1: () => (listener1Called = true),
			listener2: () => expect(expectListener1Called).toBe(listener1Called),
		};

		spyOn(listenerSpyObj, "listener1").and.callThrough();
		spyOn(listenerSpyObj, "listener2").and.callThrough();

		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1, 1);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener2, 50);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1, 100);

		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(2);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(1);

		listener1Called = false;
		expectListener1Called = false;
		testDispatcher.removeEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1);

		testDispatcher.dispatchEvent(new TestEvent(TestEvent.TEST_EVENT));

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(3);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(2);
	});

	it("stopping propagation and preventing default should work", () => {
		const testDispatcher = new TestEventDispatcher();

		const listenerSpyObj = {
			listener1: (event: TestEvent) => {
				event.preventDefault();
			},
			listener2: (event: TestEvent) => {
				event.stopPropagation();
			},
			listener3: () => {},
		};

		spyOn(listenerSpyObj, "listener1").and.callThrough();
		spyOn(listenerSpyObj, "listener2").and.callThrough();
		spyOn(listenerSpyObj, "listener3").and.callThrough();

		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener1);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener2);
		testDispatcher.addEventListener(TestEvent.TEST_EVENT, listenerSpyObj.listener3);

		const testEvent = new TestEvent(TestEvent.TEST_EVENT);

		testDispatcher.dispatchEvent(testEvent);

		expect(testEvent.isDefaultPrevented()).toBe(true);
		expect(testEvent.isPropagationStopped()).toBe(true);

		expect(listenerSpyObj.listener1).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener2).toHaveBeenCalledTimes(1);
		expect(listenerSpyObj.listener3).not.toHaveBeenCalled();
	});
});
