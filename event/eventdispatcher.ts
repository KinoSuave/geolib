import { removeElementsMatching, sortedPush } from "../collectionutil/array";
import { Disposable } from "../disposable/disposable";
import { isDef } from "../types/types";
import { Event } from "./event";
import { EventId } from "./eventid";
import { ListenerKey } from "./listenerkey";

/**
 * The EventDispatcher class is intended to be the base class for all classes that can dispatch events.
 * Functions can be registered with an EventDispatcher to be executed when an event of a particular type
 * is dispatched by the EventDispatcher, which will be given the Event object that was dispatched. Many
 * events can have their default behavior prevented, and can stop other event listeners for the same
 * event type from executing via the Event.stopPropagation() method.
 * Generally, the best way for for a class to gain event dispatching capabilities is to extend
 * EventDispatcher. However, if this is not desired, a class can create an EventDispatcher member
 * and write hooks to call methods on this member.
 */
export class EventDispatcher<B extends Event<any>> extends Disposable {
	private eventListenersMap: Map<string, IListenerBundle<B>[]> = new Map();

	/**
	 * Registers an event listener object with an EventDispatcher object so that the listener receives notification of an event.
	 *
	 * @param     {String}     type - The type of event.
	 * @param     {Function} listener - The listener function that processes the event. This function can optionally accept an Event object, which will be the dispatched event.
	 * @param     {Number}     [priority=0] - The priority level of the event listener. The higher the number, the higher the priority. All listeners with priority n are processed before listeners of priority n-1. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0.
	 * @returns {ListenerKey<T extends Event>} A key that can optionally be used to remove the event listener later.
	 */
	public addEventListener<E extends B>(type: EventId<E>, listener: (event: E) => void, priority = 0): ListenerKey<E> {
		// Prevent additions if object is disposed
		this.checkDisposalState();

		// If an event listener of this type hasn't already been added, create a new array on the _eventListeners object to keep track of events added of this type.
		let listeners = this.eventListenersMap.get(type);
		if (!isDef(listeners)) {
			listeners = [];
			this.eventListenersMap.set(type, listeners);
		}

		// Add this event listener to the listener array corresponding with its type
		const listenerKey = new ListenerKey(type, listener);

		const sortHighPriorityFirst = (a: IListenerBundle<B>, b: IListenerBundle<B>) => {
			return b.priority - a.priority;
		};

		// Keep the array in order from highest to lowest priority  (event listeners with higher priority will be executed first when an event of this type is dispatched)
		sortedPush(listeners, { priority, listenerKey: listenerKey as unknown as ListenerKey<B> }, sortHighPriorityFirst);

		return listenerKey;
	}

	/**
	 * Removes a listener from the EventDispatcher object. If there is no matching listener registered with the EventDispatcher object, a call to this method has no effect. If there are multiple instances of the listener registered with the given type, the one with the highest priority will be removed.
	 */
	public removeEventListener<E extends B>(type: EventId<E>, listener: (event: E) => void) {
		// Are there listeners of this given type registered? If not, don't worry.
		const listeners = this.eventListenersMap.get(type);
		if (isDef(listeners)) {
			// Loop through the listeners of this type from beginning to end. If one with a matching function is found, remove it from the array.
			removeElementsMatching(listeners, (bundle) => bundle.listenerKey.listener === listener, 1);
		}
	}

	/**
	 * Removes a listener from the EventDispatcher object via a key returned from the addEventListener method. If a listener matching the key has already been removed from the EventDispatcher object, a call to this method has no effect.
	 */
	public removeListenerByKey<E extends B>(listenerKey: ListenerKey<E>) {
		// Are there listeners of this given type registered? If not, don't worry.
		const listeners = this.eventListenersMap.get(listenerKey.type);
		if (isDef(listeners)) {
			// Loop through the listeners of this type from beginning to end. If one with a matching ListenerKey is found, remove it from the array.
			removeElementsMatching(
				listeners,
				(bundle) => bundle.listenerKey === (listenerKey as unknown as ListenerKey<B>),
				1
			);
		}
	}

	/**
	 * Checks whether the EventDispatcher object has any listeners registered for a specific type of event.
	 *
	 * @param type The type of event.
	 * @returns True if a listener of the specified type is registered, false otherwise
	 */
	public hasEventListener(type: EventId<B>): boolean {
		// If a key for this event type exists, and if its corresponding array isn't empty, a listener is registered
		const listeners = this.eventListenersMap.get(type);
		return isDef(listeners) && listeners.length !== 0;
	}

	/**
	 * Dispatches an event. Listeners registered for the event's type will be called and will be passed the event object.
	 *
	 * @param     {Event}     event The Event object to be dispatched.
	 * @returns {boolean} False if preventDefault() was called on the Event object, true otherwise.
	 */
	public dispatchEvent<E extends B>(event: E): boolean {
		// Prevent dispatches if object is disposed
		this.checkDisposalState();

		const listeners = this.eventListenersMap.get(event.type) as IListenerBundle<E>[] | undefined;
		if (isDef(listeners) && !event.isPropagationStopped()) {
			// Execute listeners for this event type in order
			for (const bundle of listeners) {
				// Give the listener function the Event object
				bundle.listenerKey.listener(event);

				// If propagation is ever stopped, then don't execute any more listeners
				if (event.isPropagationStopped()) {
					break;
				}
			}
		}

		return !event.isDefaultPrevented();
	}

	/**
	 * Removes references to all event listeners when disposed (prevents them from sticking around in memory).
	 * Extending classes may further override this method, but should call super.disposeInternal() when doing so.
	 */
	protected disposeInternal() {
		this.eventListenersMap = new Map();
	}

	// Throws an error if this object is disposed
	private checkDisposalState() {
		if (this.isDisposed()) {
			throw new Error("EventDispatcher is disposed");
		}
	}
}

interface IListenerBundle<B extends Event<unknown>> {
	priority: number;
	listenerKey: ListenerKey<B>;
}
