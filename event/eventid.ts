import { Brand } from "../types/brand";
import { Event } from "./event";

let counter = 0;

export type EventId<E extends Event<any>> = Brand<string, E>;

export const generateEventId = <E extends Event<any>>(eventName: string): EventId<E> => {
	const result = eventName + counter.toString();
	counter += 1;
	return result as EventId<E>;
};
