import { Event } from "./event";
import { EventId } from "./eventid";

export class ListenerKey<E extends Event<unknown>> {
	constructor(public readonly type: EventId<E>, public readonly listener: (event: E) => void) {}
}
