import { randomInteger } from "../math/randomutil";
import { isInt } from "../types/types";

export class WeightedSet<T> {
	private weightedValues: Map<T, number> = new Map();

	constructor(startingItems: Iterable<T>) {
		for (const item of startingItems) {
			this.add(item);
		}
	}

	public [Symbol.iterator](): Iterator<T> {
		return this.weightedValues.keys();
	}

	public add(value: T, weight = 1): void {
		this.checkWeight(weight);

		if (!this.weightedValues.has(value)) {
			this.weightedValues.set(value, weight);
		}
	}

	public delete(value: T): void {
		this.weightedValues.delete(value);
	}

	public entries(): IterableIterator<T> {
		return this.weightedValues.keys();
	}

	public getAllWeights(): { value: T; weight: number }[] {
		return [...this.weightedValues].map(([value, weight]) => {
			return {
				value,
				weight,
			};
		});
	}

	public getRandom(): T | undefined {
		if (this.weightedValues.size === 0) {
			return undefined;
		}

		const weightTotal = [...this.weightedValues.values()].reduce(
			(previous, current) => previous + current
		);

		const randomWeight = randomInteger(1, weightTotal);

		let remainingWeight = randomWeight;
		for (const [value, weight] of this.weightedValues) {
			remainingWeight -= weight;

			if (remainingWeight <= 0) {
				return value;
			}
		}

		return undefined;
	}

	public getWeight(value: T): number | undefined {
		return this.weightedValues.get(value);
	}

	public has(value: T): boolean {
		return this.weightedValues.has(value);
	}

	public set(value: T, weight: number): void {
		this.checkWeight(weight);
		this.weightedValues.set(value, weight);
	}

	private checkWeight(weight: number): void {
		if (!isInt(weight) && weight > 0) {
			throw new Error(`Weight ${weight} is not a positive integer`);
		}
	}
}
