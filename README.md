The Geo Library is a TypeScript library containing helpful classes and utilities. Developed by Brant Nielsen for use in a wide variety of projects.

Originally started with a TypeScript implementation of Brant's JavaScript-based EventDispatcher, which was inspired from ActionScript 3 and later influenced by the Google Closure Library and others. The original JavaScript version of this class was used in the HammerLMS project, and was converted to TypeScript with the intention of being used in Kunren and other new projects.

To run Jasmine tests (which will run all tests ending in *.spec.ts), run `npm test` after installing all dependencies.