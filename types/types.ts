export const isDef = <T>(val: T | undefined): val is T => {
	return val !== undefined;
};

export const isDefAndNotNull = <T>(val: T | null | undefined): val is T => {
	return val !== null && val !== undefined;
};

export const isString = (val: unknown): val is string => {
	return typeof val === "string";
};

export const isBoolean = (val: unknown): val is boolean => {
	return typeof val === "boolean";
};

export const isNumber = (val: unknown): val is number => {
	return typeof val === "number";
};

export const isNumberAndNotNaN = (val: unknown): val is number => {
	return isNumber(val) && !isNaN(val);
};

export const isInt = (val: unknown): val is number => {
	return isNumber(val) && isFinite(val) && val % 1 === 0;
};

export const isFunction = (
	val: unknown
): val is (...unknownArgs: any[]) => unknown => {
	return typeof val === "function";
};

export const isArray = (val: unknown): val is unknown[] => {
	return Array.isArray(val);
};

export const isArrayOfType = <T>(
	val: unknown,
	typeGuard: (a: unknown) => a is T
): val is T[] => {
	return isArray(val) && val.every(typeGuard);
};
