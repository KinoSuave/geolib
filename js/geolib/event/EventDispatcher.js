/**
 * The EventDispatcher class is intended to be the base class for all classes that can dispatch events.
 * Functions can be registered with an EventDispatcher to be executed when an event of a particular type
 * is dispached by the EventDispatcher, which will be given the Event object that was dispatched. Many
 * events can have their default behavior prevented, and can stop other event listeners for the same
 * event type from executing via the Event.stopPropagation() method.
 * Generally, the best way for for a class to gain event dispatching capabilities is to extend
 * EventDispatcher. However, if this is not desired, a class can create an EventDispatcher member
 * and write hooks to call methods on this member.
 *
 * @author Brant Nielsen
 */
define(function () {
	/**
     * Creates an instance of EventDispatcher
     *
	 * @constructor
	 */
    function EventDispatcher() {
        //Keeps track of all event listeners by type (each key in this object will correlate to an array of event listeners sorted by priority)
		/** @private */ this._eventListeners = {};
    }

	/**
	 * Registers an event listener object with an EventDispatcher object so that the listener receives notification of an event.
	 * @param   {String}   type - The type of event.
	 * @param   {Function} listener - The listener function that processes the event. This function can optionally accept an Event object, which will be the dispatched event.
	 * @param   {Number}   [priority=0] - The priority level of the event listener. The higher the number, the higher the priority. All listeners with priority n are processed before listeners of priority n-1. If two or more listeners share the same priority, they are processed in the order in which they were added. The default priority is 0.
	 */
    EventDispatcher.prototype.addEventListener = function (type, listener, priority) {
        //Check that type is a string
        if (typeof type !== "string") {
            throw "An event listener type must be a string.";
        }

        //Check that listener is a function
        if (typeof listener !== "function") {
            throw "An event listener must be a function.";
        }

        //Default priority to 0 if it wasn't specified
        if (typeof priority !== "number") {
            priority = 0;
        }

        //If an event listener of this type hasn't already been added, create a new array on the _eventListeners object to keep track of events added of this type.
        if (typeof this._eventListeners[type] === "undefined") {
            this._eventListeners[type] = [];
        }

        //Add this event listener to the listener array corresponding with its type
        var listeners = this._eventListeners[type];
        listeners.push({ 'listener': listener, 'priority': priority });

        //Sort the array from highest to lowest priority (event listeners with higher priority will be executed first when an event of this type is dispatched)
        listeners.sort(sortHighPriorityFirst);

        function sortHighPriorityFirst(a, b) {
            return b.priority - a.priority;
        };
    };

	/**
	 * Removes a listener from the EventDispatcher object. If there is no matching listener registered with the EventDispatcher object, a call to this method has no effect. If there are multiple instances of the listener registered with the given type, the one with the highest priority will be removed.
	 * @param {String}   type     The type of event.
	 * @param {Function} listener The listener function to remove.
	 */
    EventDispatcher.prototype.removeEventListener = function (type, listener) {
        //Are there listeners of this given type registered? If not, don't worry.
        if (typeof this._eventListeners[type] !== "undefined") {
            //Loop through the listeners of this type from beginning to end. If one with a matching function is found, remove it from the array.
            var listeners = this._eventListeners[type];
            var iEnd = listeners.length;
            for (var i = 0; i < iEnd; i++) {
                if (listeners[i].listener === listener) {
                    listeners.splice(i, 1);
                    //Once we've found one, stop searching
                    return;
                }
            }
        }
    };

	/**
	 * Checks whether the EventDispatcher object has any listeners registered for a specific type of event.
	 * @param   {String}  type The type of event.
	 * @returns {Boolean} True if a listener of the specified type is registered, false otherwise
	 */
    EventDispatcher.prototype.hasEventListener = function (type) {
        //If a key for this event type exists, and if its corresponding array isn't empty, a listener is registered
        return (typeof this._eventListeners[type] !== "undefined" && this._eventListeners[type].length !== 0);
    };

	/**
	 * Dispatches an event. Listeners registered for the event's type will be called and will be passed the event object.
	 * @param   {Event}   event The Event object to be dispatched.
	 * @returns {Boolean} False if preventDefault() was called on the Event object, true otherwise.
	 */
    EventDispatcher.prototype.dispatchEvent = function (event) {
        if (typeof this._eventListeners[event.type] !== "undefined" && !event.isPropagationStopped()) {
            //Get all listeners for this event type and execute them in order
            var listeners = this._eventListeners[event.type];

            var iEnd = listeners.length;
            for (var i = 0; i < iEnd; i++) {
                //Give the listener function the Event object
                listeners[i].listener(event);
                //If propagation is ever stopped, then don't execute any more listeners
                if (event.isPropagationStopped()) {
                    break;
                }
            }
        }

        //Return whether the default behavior should be performed.
        return !event.isDefaultPrevented();
    };

    return EventDispatcher;
});