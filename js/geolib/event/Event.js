define(function () {

    //Creates a new event with the specified type
    function Event(type) {
        this.type = type;
        this._defaultPrevented = false;
        this._propagationStopped = false;
    };

    //Cancels an event's default behavior if that behavior can be canceled.
    Event.prototype.preventDefault = function () {
        this._defaultPrevented = true;
    };

    //Checks whether the preventDefault() method has been called on the event.
    Event.prototype.isDefaultPrevented = function () {
        return this._defaultPrevented;
    };

    //Prevents processing of any subsequent event listener in the event flow.
    Event.prototype.stopPropagation = function () {
        this._propagationStopped = true;
    };

    //Checks whether the stopPropagation() method has been called on the event.
    Event.prototype.isPropagationStopped = function () {
        return this._propagationStopped;
    };

    return Event;
});