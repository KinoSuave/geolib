/** Concrete Id class that can be used to give different kinds of IDs different types, so they don't get mixed up. */
export abstract class Id<T extends string | number> {
	constructor(public readonly raw: T) {}

	public equals(other: Id<T>): boolean {
		return this.raw === other.raw;
	}

	public compareTo(other: Id<T>): number {
		if (this.raw > other.raw) {
			return 1;
		}

		if (this.raw < other.raw) {
			return -1;
		}

		return 0;
	}

	public toString(): string {
		return this.raw.toString();
	}

	public toJson(): T {
		return this.raw;
	}
}
