/**
 * Disposable class modeled somewhat after the Closure Library Disposable class. Useful for
 * preventing memory leaks by allowing any extending classes to clean up their references
 * on disposal.
 *
 * @author Brant Nielsen
 */
export abstract class Disposable {
	private _disposed = false;
	private _callbacks: Callback[] = [];

	/**
	 * Disposes of this object and calls all relevant dispose callbacks.
	 */
	dispose() {
		this._disposed = true;

		this._callbacks.forEach((callback) => {
			callback();
		});

		// Release all callback references
		this._callbacks = [];

		this.disposeInternal();
	}

	/**
	 * True if this object has been disposed.
	 */
	isDisposed(): boolean {
		return this._disposed;
	}

	/**
	 * Adds a callback to be invoked when the object is disposed.
	 *
	 * @param callback Callback function
	 */
	addOnDisposeCallback(callback: Callback): void {
		this._callbacks.push(callback);
	}

	/**
	 * Extending classes should override this method to perform necessary cleanup
	 */
	protected abstract disposeInternal(): void;
}

type Callback = () => unknown;
