export const randomInteger = (min: number, max: number): number => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const randomReal = (min: number, max: number): number => {
	return Math.random() * (max - min) + min;
};

export const randomBoolean = (percentage = 0.5): boolean => {
	return Math.random() < percentage;
};
