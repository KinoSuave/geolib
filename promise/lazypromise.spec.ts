import "jasmine";
import { LazyPromise } from "./lazypromise";

describe("LazyPromise", () => {
	it("should only execute the generator once when awaited", async () => {
		let generatorRunTimes = 0;
		const lazyPromise = new LazyPromise(() => {
			generatorRunTimes++;
			return 5;
		});

		expect(generatorRunTimes).toBe(0);

		const result = await lazyPromise;

		expect(result).toBe(5);
		expect(generatorRunTimes).toBe(1);

		const nextResult = await lazyPromise;

		expect(nextResult).toBe(5);
		expect(generatorRunTimes).toBe(1);
	});

	it("should work with normal promises", async () => {
		const lazyPromise = new LazyPromise(() => Promise.resolve(3));

		const result = await lazyPromise;

		expect(result).toBe(3);
	});

	it("should have correct catch behavior", async () => {
		const lazyPromise = new LazyPromise(() => {
			return Promise.reject("Didn't work");
		});

		let failed = false;

		try {
			await lazyPromise;
		} catch {
			failed = true;
		}

		expect(failed).toBeTrue();

		await expectAsync(lazyPromise).toBeRejected();
	});

	it("should reject promise if the generator throws", async () => {
		const lazyPromise = new LazyPromise(() => {
			throw new Error("Didn't work");
		});

		await expectAsync(lazyPromise).toBeRejected();
	});

	it("should have proper finally behavior", async () => {
		const lazyPromise = new LazyPromise(() => {
			throw new Error("Didn't work");
		});

		let finallyRan = false;

		try {
			await lazyPromise;
		} catch {
		} finally {
			finallyRan = true;
		}

		expect(finallyRan).toBeTrue();
	});
});
