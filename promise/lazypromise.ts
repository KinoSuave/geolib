import { isDef } from "../types/types";

type PromiseGenerator<T> = (() => Promise<T>) | (() => T);

export class LazyPromise<T> implements Promise<T> {
	[Symbol.toStringTag] = "Promise";

	private result: Promise<T> | undefined;

	constructor(private promiseGenerator: PromiseGenerator<T>) {}

	private getPromise(): Promise<T> {
		if (!isDef(this.result)) {
			let promise: T | Promise<T>;

			try {
				promise = this.promiseGenerator();
			} catch (error: unknown) {
				promise = Promise.reject(error);
			}

			if (typeof (promise as Promise<T>).then === "function") {
				this.result = promise as Promise<T>;
			} else {
				this.result = Promise.resolve(promise);
			}
			// Garbage collect the generator, since we're done with it.
			this.promiseGenerator = undefined as unknown as PromiseGenerator<T>;
		}

		return this.result;
	}

	public then<TResult1 = T, TResult2 = never>(
		onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | null | undefined,
		onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | null | undefined
	): Promise<TResult1 | TResult2> {
		return this.getPromise().then(onfulfilled, onrejected);
	}

	public catch<TResult = never>(
		onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | null | undefined
	): Promise<T | TResult> {
		return this.getPromise().catch(onrejected);
	}

	public finally(onfinally?: (() => void) | null | undefined): Promise<T> {
		return this.getPromise().finally(onfinally);
	}
}
