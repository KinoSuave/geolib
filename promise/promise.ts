/**
 * Throttles the amount of promises that can be in flight at any given time. Returns
 * an array containing the results of all generated promises in the order the promises
 * were generated in. If a promise rejected, the corresponding result in the array will
 * be undefined. If you need the result of a reject, then wrap the promise via the generator
 * and catch there.
 *
 * @param promiseGenerator Generates promises that will be executed in order
 * @param maxInFlight The maximum number of promises that can be in flight at once
 */

import { isDef } from "../types/types";

// eslint-disable-next-line import/prefer-default-export
export const throttledResolve = async <T>(
	promiseGenerator: Generator<Promise<T>>,
	maxInFlight: number
): Promise<(T | undefined)[]> => {
	const resultArray: T[] = [];
	let i = 0;

	const doNextAction = async (): Promise<void> => {
		const thisAction = promiseGenerator.next();

		if (!isDef(thisAction.done) || !thisAction.done) {
			const thisResultIndex = i;
			i++;

			// Don't abort on a reject. If catching the result of a reject is needed,
			// wrap the action via the generator and catch in the wrapper.
			try {
				const result = (await thisAction.value) as T;
				resultArray[thisResultIndex] = result;
			} catch {}

			await doNextAction();
		}
	};

	// The concept here essentially is that every entry in this array represents
	// a "slot" that can be used to execute whatever actions are next. When any one
	// slot finishes, it takes on the next action, and doesn't resolve until there
	// are no more actions left to take. This recursive calling strategy means that
	// one slot could take on any indeterminate number of actions, including none
	// at all (in the case that there are fewer actions than the maxInFlight limit)
	const promiseSlots: Promise<void>[] = [];

	for (let promiseSlotNum = 0; promiseSlotNum < maxInFlight; promiseSlotNum++) {
		promiseSlots.push(doNextAction());
	}

	await Promise.all(promiseSlots);
	return resultArray;
};
