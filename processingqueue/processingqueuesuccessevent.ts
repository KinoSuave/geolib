import { generateEventId } from "../event/eventid";
import { ProcessingQueueEvent } from "./processingqueueevent";

export class ProcessingQueueSuccessEvent<
	QUEUE_ITEM_TYPE,
	RESULT_ITEM_TYPE
> extends ProcessingQueueEvent {
	public static readonly eventId = generateEventId<ProcessingQueueEvent>(
		"PROCESS_QUEUE_SUCCESS_EVENT"
	);

	constructor(
		public readonly queuedItem: QUEUE_ITEM_TYPE,
		public readonly result: RESULT_ITEM_TYPE
	) {
		super(ProcessingQueueSuccessEvent.eventId, undefined);
	}
}
