import { generateEventId } from "../event/eventid";
import { ProcessingQueueEvent } from "./processingqueueevent";

export class ProcessingQueueFailureEvent<
	QUEUE_ITEM_TYPE
> extends ProcessingQueueEvent {
	public static readonly eventId = generateEventId<ProcessingQueueEvent>(
		"PROCESS_QUEUE_FAILURE_EVENT"
	);

	constructor(
		public readonly queuedItem: QUEUE_ITEM_TYPE,
		public readonly error: unknown
	) {
		super(ProcessingQueueFailureEvent.eventId, undefined);
	}
}
