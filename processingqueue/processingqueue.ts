import * as GeoArray from "../collectionutil/array";
import { EventDispatcher } from "../event/eventdispatcher";
import { isDef, isInt } from "../types/types";
import { ProcessingQueueEvent } from "./processingqueueevent";
import { ProcessingQueueFailureEvent } from "./processingqueuefailureevent";
import { ProcessingQueueSuccessEvent } from "./processingqueuesuccessevent";

/**
 * Represents a queue with commands that get processed one at a time. To use, extend this class and override the abstract processItem() method.
 * Enqueued items are processed in the order that they are inserted. Items can be added sooner in the queue by inserting them at a non-negative integer position, with lower numbers being inserted earlier in the queue than higher numbers.
 * Queue progress can either be monitored by using the enqueueAndWait() methods and awaiting the returned promises, or by listening for fired {@link ProcessingQueueSuccessEvent} and {@link ProcessingQueueFailureEvent} events on the class itself.
 */
export abstract class ProcessingQueue<
	QUEUE_ITEM_TYPE,
	RESULT_ITEM_TYPE
> extends EventDispatcher<ProcessingQueueEvent> {
	private readonly dequeueError = new Error("Item has been dequeued.");
	private queue: IQueueItem<QUEUE_ITEM_TYPE, RESULT_ITEM_TYPE>[] = [];
	private processingQueue = false;

	/**
	 * Creates a ProcessingQueue.
	 *
	 * @param preventDuplicates - Ignore duplicate items added to the queue.
	 */
	constructor(private readonly preventDuplicates: boolean = false) {
		super();

		this.addOnDisposeCallback(() => this.clearQueue());
	}

	/** Clears the whole queue. Processing on the current item will finish, but no more in the existing queue will finish. */
	public clearQueue(): void {
		const clearedItems = this.queue;
		this.queue = [];
		this.rejectQueueItemsOnDequeue(clearedItems);
	}

	/** Returns the number of unprocessed items in the queue. */
	public size(): number {
		return this.queue.length;
	}

	/** Dequeues a single item from the queue. Returns true if a matching item was found and dequeued. */
	public dequeueOne(item: QUEUE_ITEM_TYPE): boolean {
		const removedItems = GeoArray.removeElementsMatching(
			this.queue,
			(queueItem) => queueItem.item === item,
			1
		);

		this.rejectQueueItemsOnDequeue(removedItems);

		return removedItems.length > 0;
	}

	/** Dequeues all items that stricly equal the provided item. Returns the number of items dequeued. */
	public dequeueAll(item: QUEUE_ITEM_TYPE): number {
		const removedItems = GeoArray.removeElementsMatching(
			this.queue,
			(queueItem) => queueItem.item === item
		);

		this.rejectQueueItemsOnDequeue(removedItems);

		return removedItems.length;
	}

	/** Enqueues a single item. */
	public enqueue(item: QUEUE_ITEM_TYPE, position?: number | undefined): void {
		this.checkPosition(position);

		// Prevent uncaught promise errors
		this.enqueueAndWait(item, position).catch(() => {});
	}

	/** Enqueues many items at once. */
	public enqueueAll(
		items: QUEUE_ITEM_TYPE[],
		position?: number | undefined
	): void {
		this.checkPosition(position);

		// Prevent uncaught promise errors
		this.enqueueAllAndWait(items, position).catch(() => {});
	}

	/** Enqueues an item, and returns a Promise that resolves with the processing result when the item is processed. Rejects on failure to process, or if the item becomes dequeued. If preventDuplicates is on, then if the item already exists in the queue, will resolve when that one is processed with its result. */
	public enqueueAndWait(
		item: QUEUE_ITEM_TYPE,
		position?: number | undefined
	): Promise<RESULT_ITEM_TYPE> {
		return new Promise((resolve, reject) => {
			this.checkPosition(position);

			let queueItem: IQueueItem<QUEUE_ITEM_TYPE, RESULT_ITEM_TYPE> | undefined;
			if (this.preventDuplicates) {
				queueItem = this.findQueueItem(item);
			}

			if (!isDef(queueItem)) {
				queueItem = {
					item,
					resolveListeners: [],
					rejectListeners: [],
				};

				if (isDef(position)) {
					GeoArray.insertAt(
						this.queue,
						Math.min(position, this.queue.length),
						queueItem
					);
				} else {
					this.queue.push(queueItem);
				}
			}

			queueItem.resolveListeners.push(resolve);
			queueItem.rejectListeners.push(reject);

			void this.runQueueIfNotProcessing();
		});
	}

	/** Enqueues many items, and returns a Promise that resolves when all of the items are processed. Rejects if any of them fail, or if any of them are dequeued. */
	public enqueueAllAndWait(
		items: QUEUE_ITEM_TYPE[],
		position?: number | undefined
	): Promise<RESULT_ITEM_TYPE[]> {
		try {
			this.checkPosition(position);
		} catch (error) {
			return Promise.reject(error);
		}

		const itemsInOrder = isDef(position) ? items.reverse() : items;

		const promises = itemsInOrder.map((item) =>
			this.enqueueAndWait(item, position)
		);

		return Promise.all(promises);
	}

	/** Finds the position of the given item in the queue. If there are many of the same item in the queue, returns the position of the first one. */
	public findPosition(item: QUEUE_ITEM_TYPE): number {
		return this.queue.findIndex((queueItem) => queueItem.item === item);
	}

	/** Gets queued items in the order that they'll be processed. Any items currently being processed are not included. */
	public getQueued(): QUEUE_ITEM_TYPE[] {
		return this.queue.map((queueItem) => queueItem.item);
	}

	/** Returns true if the given item is in the queue at least once. */
	public includes(item: QUEUE_ITEM_TYPE): boolean {
		return this.findPosition(item) > -1;
	}

	/** Returns a Promise that resolves when the given item in the queue is processed. If the item is not in the queue, resolves immediately with undefined. If there are multiples of the given item in the queue, resolves with the result of the first item in the queue. */
	public waitFor(item: QUEUE_ITEM_TYPE): Promise<RESULT_ITEM_TYPE | undefined> {
		return new Promise((resolve, reject) => {
			const queuedItem = this.findQueueItem(item);

			if (isDef(queuedItem)) {
				queuedItem.resolveListeners.push(resolve);
				queuedItem.rejectListeners.push(reject);
			} else {
				resolve(undefined);
			}
		});
	}

	protected abstract processItem(
		item: QUEUE_ITEM_TYPE
	): Promise<RESULT_ITEM_TYPE>;

	private async runQueueIfNotProcessing(): Promise<void> {
		if (this.processingQueue) {
			return;
		}

		this.processingQueue = true;
		while (this.queue.length > 0) {
			const currentQueueItem = this.queue.shift();

			if (isDef(currentQueueItem)) {
				try {
					const result = await this.processItem(currentQueueItem.item);

					currentQueueItem.resolveListeners.forEach((resolve) =>
						resolve(result)
					);
					this.dispatchEvent(
						new ProcessingQueueSuccessEvent(currentQueueItem.item, result)
					);
				} catch (error) {
					currentQueueItem.rejectListeners.forEach((reject) => reject(error));
					this.dispatchEvent(
						new ProcessingQueueFailureEvent(currentQueueItem.item, error)
					);
				}
			}
		}

		this.processingQueue = false;
	}

	private rejectQueueItemsOnDequeue(
		queueItems: IQueueItem<QUEUE_ITEM_TYPE, RESULT_ITEM_TYPE>[]
	): void {
		queueItems.forEach((queueItem) => {
			queueItem.rejectListeners.forEach((reject) => reject(this.dequeueError));
		});
	}

	private findQueueItem(
		item: QUEUE_ITEM_TYPE
	): IQueueItem<QUEUE_ITEM_TYPE, RESULT_ITEM_TYPE> | undefined {
		return this.queue.find((searchItem) => searchItem.item === item);
	}

	private checkPosition(position: number | undefined): void {
		if (isDef(position) && (position < 0 || !isInt(position))) {
			throw new Error(`Position ${position} is not a non-negative integer`);
		}
	}
}

interface IQueueItem<Q, R> {
	item: Q;
	resolveListeners: ((result: R) => void)[];
	rejectListeners: ((reason?: any) => void)[];
}
